public class PlusOperator extends BinaryOperator {

    public PlusOperator(String sign) {
        super(sign);
    }

    @Override
    public double apply(double firstOperand, double secondOperand) {
        return firstOperand + secondOperand;
    }

}
