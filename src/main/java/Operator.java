import java.util.Stack;

public abstract class Operator {

    private final String sign;

    protected Operator(String sign) {
        this.sign = sign;
    }

    public String getSign() {
        return sign;
    }

    public abstract double executeUsing(Stack<Double> operandStack);
}
