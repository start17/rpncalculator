import java.util.*;

public class RPNCalculator {

    private Map<String, Operator> operators = new HashMap<>();

    public RPNCalculator(List<Operator> operatorList) {

        for (Operator operator : operatorList) {
            operators.put(operator.getSign(), operator);
        }
    }

    public double calculate(String postfixExpression) {

        String[] postfixExpressionTokens = postfixExpression.split("\\s");
        Stack<Double> operandStack = new Stack<>();

        for (String token : postfixExpressionTokens) {

            Operator operator = operators.get(token);

            if (operator == null) {
                operandStack.push(new Double(token));
            } else {
                double calculatedValue = operator.executeUsing(operandStack);
                operandStack.push(calculatedValue);
            }
        }

        return operandStack.pop();
    }

    public static void main(String[] args) {

        ArrayList<Operator> operatorList = new ArrayList<>();
        operatorList.add(new PlusOperator("+"));
        operatorList.add(new MinusOperator("-"));
        operatorList.add(new MultiplicationOperator("*"));
        operatorList.add(new DivisionOpertaor("/"));
        operatorList.add(new ExponentOperator("^"));
        operatorList.add(new PercentOperator("%"));
        operatorList.add(new FactorialOperator("!"));

        RPNCalculator rpnCalculator = new RPNCalculator(operatorList);

        System.out.println("1 2 3 + -  :: "+ rpnCalculator.calculate("1 2 3 + -"));
        System.out.println("6 2 * 3 /  :: "+ rpnCalculator.calculate("6 2 * 3 /"));
        System.out.println("2 3 ^ 4 5 + +  :: "+ rpnCalculator.calculate("2 3 ^ 4 5 + +"));
        System.out.println("50 % 2 *  :: "+ rpnCalculator.calculate("50 % 2 *"));
        System.out.println("3 ! 4 5 * +  :: "+ rpnCalculator.calculate("3 ! 4 5 * +"));
        System.out.println("12 3 / !  :: "+ rpnCalculator.calculate("12 3 / !"));
        System.out.println("5 1 2 + 4 * + 3 -  :: "+ rpnCalculator.calculate("5 1 2 + 4 * + 3 -"));
    }
}

