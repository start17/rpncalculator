public class MultiplicationOperator extends BinaryOperator {

    public MultiplicationOperator(String sign) {
        super(sign);
    }

    @Override
    public double apply(double firstOperand, double secondOperand) {
        return firstOperand * secondOperand;
    }
}
