import java.util.Stack;

public abstract class UnaryOperator extends Operator {

    protected UnaryOperator(String sign) {
        super(sign);
    }

    @Override
    public double executeUsing(Stack<Double> operandStack) {
        return apply(operandStack.pop());
    }

    public abstract double apply(double operand);
}
