public class DivisionOpertaor extends BinaryOperator {

    public DivisionOpertaor(String sign) {
        super(sign);
    }

    @Override
    public double apply(double firstOperand, double secondOperand) {
        return firstOperand / secondOperand;
    }
}
