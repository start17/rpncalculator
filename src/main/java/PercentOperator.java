public class PercentOperator extends UnaryOperator {

    public PercentOperator(String sign) {
        super(sign);
    }

    @Override
    public double apply(double operand) {
        return operand/100;
    }
}
