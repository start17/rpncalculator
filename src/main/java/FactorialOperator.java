public class FactorialOperator extends UnaryOperator {

    public FactorialOperator(String sign) {
        super(sign);
    }

    @Override
    public double apply(double operand) {

        double factorial = 1;
        while (operand > 1) {
            factorial *= operand;
            operand--;
        }

        return factorial;
    }
}
