public class MinusOperator extends BinaryOperator {

    public MinusOperator(String sign) {
        super(sign);
    }

    @Override
    public double apply(double firstOperand, double secondOperand) {
        return firstOperand - secondOperand;
    }
}
