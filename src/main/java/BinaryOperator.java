import java.util.Stack;

public abstract class BinaryOperator extends Operator {

    protected BinaryOperator(String sign) {
        super(sign);
    }

    public double executeUsing(Stack<Double> operandStack) {
        Double secondOperand = operandStack.pop();
        Double firstOperand = operandStack.pop();

        return apply(firstOperand, secondOperand);
    }

    public abstract double apply(double firstOperand, double secondOperand);
}
