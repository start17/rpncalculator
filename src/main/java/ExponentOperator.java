public class ExponentOperator extends BinaryOperator {

    public ExponentOperator(String sign) {
        super(sign);
    }

    @Override
    public double apply(double firstOperand, double secondOperand) {
        return Math.pow(firstOperand, secondOperand);
    }
}
