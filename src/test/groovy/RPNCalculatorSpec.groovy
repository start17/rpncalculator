import spock.lang.Specification

class RPNCalculatorSpec extends Specification {

    List<Operator> operators = [
            new PlusOperator("+"),
            new MinusOperator("-"),
            new MultiplicationOperator("*"),
            new DivisionOpertaor("/"),
            new ExponentOperator("^"),
            new PercentOperator("%"),
            new FactorialOperator("!")
    ]

    RPNCalculator rpnCalculator = new RPNCalculator(operators)

    def 'should calculate correct value for a given postfix expression'() {

        when:
        double result = rpnCalculator.calculate(postfixExpression)

        then:
        result == expectedResult

        where:
        postfixExpression   | expectedResult
        '1 2 3 + -'         | -4.0d
        '6 2 * 3 /'         | 4.0d
        '2 3 ^ 4 5 + +'     | 17.0d
        '50 % 2 *'          | 1.0d
        '3 ! 4 5 * +'       | 26.0d
        '12 3 / !'          | 24.0d
        '5 1 2 + 4 * + 3 -' | 14.0d
    }
}
